# ServerConfigCustomTranscoding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] 
**AllowAdditionalExtensions** | **bool** |  | [optional] 
**AllowAudioFiles** | **bool** |  | [optional] 
**Threads** | **float32** |  | [optional] 
**Resolutions** | [**ServerConfigCustomTranscodingResolutions**](ServerConfigCustom_transcoding_resolutions.md) |  | [optional] 
**Hls** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


