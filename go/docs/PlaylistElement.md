# PlaylistElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Position** | **float32** |  | [optional] 
**StartTimestamp** | **float32** |  | [optional] 
**StopTimestamp** | **float32** |  | [optional] 
**Video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


