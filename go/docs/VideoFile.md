# VideoFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MagnetUri** | **string** |  | [optional] 
**Resolution** | [**VideoResolutionConstant**](VideoResolutionConstant.md) |  | [optional] 
**Size** | **float32** | Video file size in bytes | [optional] 
**TorrentUrl** | **string** |  | [optional] 
**TorrentDownloadUrl** | **string** |  | [optional] 
**FileUrl** | **string** |  | [optional] 
**FileDownloadUrl** | **string** |  | [optional] 
**Fps** | **float32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


