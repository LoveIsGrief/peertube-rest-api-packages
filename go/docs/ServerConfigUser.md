# ServerConfigUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VideoQuota** | **float32** |  | [optional] 
**VideoQuotaDaily** | **float32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


