# VideoAbuse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Reason** | **string** |  | [optional] 
**ReporterAccount** | [**Account**](Account.md) |  | [optional] 
**Video** | [**VideoAbuseVideo**](VideoAbuse_video.md) |  | [optional] 
**CreatedAt** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


