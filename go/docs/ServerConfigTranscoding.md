# ServerConfigTranscoding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hls** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 
**EnabledResolutions** | **[]float32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


