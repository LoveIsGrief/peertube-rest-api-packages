# \VideoBlacklistApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosBlacklistGet**](VideoBlacklistApi.md#VideosBlacklistGet) | **Get** /videos/blacklist | Get list of videos on blacklist
[**VideosIdBlacklistDelete**](VideoBlacklistApi.md#VideosIdBlacklistDelete) | **Delete** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
[**VideosIdBlacklistPost**](VideoBlacklistApi.md#VideosIdBlacklistPost) | **Post** /videos/{id}/blacklist | Put on blacklist a video by its id



## VideosBlacklistGet

> []VideoBlacklist VideosBlacklistGet(ctx, optional)

Get list of videos on blacklist

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideosBlacklistGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosBlacklistGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort blacklists by criteria | 

### Return type

[**[]VideoBlacklist**](VideoBlacklist.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdBlacklistDelete

> VideosIdBlacklistDelete(ctx, id)

Delete an entry of the blacklist of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdBlacklistPost

> VideosIdBlacklistPost(ctx, id)

Put on blacklist a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

