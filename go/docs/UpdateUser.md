# UpdateUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The user id  | 
**Email** | **string** | The updated email of the user  | 
**VideoQuota** | **string** | The updated videoQuota of the user  | 
**VideoQuotaDaily** | **string** | The updated daily video quota of the user  | 
**Role** | **int32** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


