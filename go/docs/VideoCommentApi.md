# \VideoCommentApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VideosIdCommentThreadsGet**](VideoCommentApi.md#VideosIdCommentThreadsGet) | **Get** /videos/{id}/comment-threads | Get the comment threads of a video by its id
[**VideosIdCommentThreadsPost**](VideoCommentApi.md#VideosIdCommentThreadsPost) | **Post** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
[**VideosIdCommentThreadsThreadIdGet**](VideoCommentApi.md#VideosIdCommentThreadsThreadIdGet) | **Get** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
[**VideosIdCommentsCommentIdDelete**](VideoCommentApi.md#VideosIdCommentsCommentIdDelete) | **Delete** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
[**VideosIdCommentsCommentIdPost**](VideoCommentApi.md#VideosIdCommentsCommentIdPost) | **Post** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id



## VideosIdCommentThreadsGet

> CommentThreadResponse VideosIdCommentThreadsGet(ctx, id, optional)

Get the comment threads of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
 **optional** | ***VideosIdCommentThreadsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosIdCommentThreadsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort comments by criteria | 

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentThreadsPost

> CommentThreadPostResponse VideosIdCommentThreadsPost(ctx, id)

Creates a comment thread, on a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentThreadsThreadIdGet

> VideoCommentThreadTree VideosIdCommentThreadsThreadIdGet(ctx, id, threadId)

Get the comment thread by its id, of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
**threadId** | **float32**| The thread id (root comment id) | 

### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentsCommentIdDelete

> VideosIdCommentsCommentIdDelete(ctx, id, commentId)

Delete a comment in a comment thread by its id, of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
**commentId** | **float32**| The comment id | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdCommentsCommentIdPost

> CommentThreadPostResponse VideosIdCommentsCommentIdPost(ctx, id, commentId)

Creates a comment in a comment thread by its id, of a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
**commentId** | **float32**| The comment id | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

