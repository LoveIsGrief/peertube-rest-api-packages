# ServerConfigCustom

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Instance** | [**ServerConfigCustomInstance**](ServerConfigCustom_instance.md) |  | [optional] 
**Theme** | [**ServerConfigCustomTheme**](ServerConfigCustom_theme.md) |  | [optional] 
**Services** | [**ServerConfigCustomServices**](ServerConfigCustom_services.md) |  | [optional] 
**Cache** | [**ServerConfigCustomCache**](ServerConfigCustom_cache.md) |  | [optional] 
**Signup** | [**ServerConfigCustomSignup**](ServerConfigCustom_signup.md) |  | [optional] 
**Admin** | [**ServerConfigCustomAdmin**](ServerConfigCustom_admin.md) |  | [optional] 
**ContactForm** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 
**User** | [**ServerConfigUser**](ServerConfig_user.md) |  | [optional] 
**Transcoding** | [**ServerConfigCustomTranscoding**](ServerConfigCustom_transcoding.md) |  | [optional] 
**Import** | [**ServerConfigImport**](ServerConfig_import.md) |  | [optional] 
**AutoBlacklist** | [**ServerConfigAutoBlacklist**](ServerConfig_autoBlacklist.md) |  | [optional] 
**Followers** | [**ServerConfigCustomFollowers**](ServerConfigCustom_followers.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


