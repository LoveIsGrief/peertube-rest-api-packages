# VideoStreamingPlaylists

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Type** | **float32** | Playlist type (HLS &#x3D; 1) | [optional] 
**PlaylistUrl** | **string** |  | [optional] 
**SegmentsSha256Url** | **string** |  | [optional] 
**Redundancies** | [**[]VideoStreamingPlaylistsRedundancies**](VideoStreamingPlaylists_redundancies.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


