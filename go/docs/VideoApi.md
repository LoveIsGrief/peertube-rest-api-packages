# \VideoApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountsNameVideosGet**](VideoApi.md#AccountsNameVideosGet) | **Get** /accounts/{name}/videos | Get videos for an account, provided the name of that account
[**VideoChannelsChannelHandleVideosGet**](VideoApi.md#VideoChannelsChannelHandleVideosGet) | **Get** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**VideosCategoriesGet**](VideoApi.md#VideosCategoriesGet) | **Get** /videos/categories | Get list of video categories known by the server
[**VideosGet**](VideoApi.md#VideosGet) | **Get** /videos | Get list of videos
[**VideosIdDelete**](VideoApi.md#VideosIdDelete) | **Delete** /videos/{id} | Delete a video by its id
[**VideosIdDescriptionGet**](VideoApi.md#VideosIdDescriptionGet) | **Get** /videos/{id}/description | Get a video description by its id
[**VideosIdGet**](VideoApi.md#VideosIdGet) | **Get** /videos/{id} | Get a video by its id
[**VideosIdGiveOwnershipPost**](VideoApi.md#VideosIdGiveOwnershipPost) | **Post** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
[**VideosIdPut**](VideoApi.md#VideosIdPut) | **Put** /videos/{id} | Update metadata for a video by its id
[**VideosIdViewsPost**](VideoApi.md#VideosIdViewsPost) | **Post** /videos/{id}/views | Add a view to the video by its id
[**VideosIdWatchingPut**](VideoApi.md#VideosIdWatchingPut) | **Put** /videos/{id}/watching | Set watching progress of a video by its id for a user
[**VideosImportsPost**](VideoApi.md#VideosImportsPost) | **Post** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
[**VideosLanguagesGet**](VideoApi.md#VideosLanguagesGet) | **Get** /videos/languages | Get list of languages known by the server
[**VideosLicencesGet**](VideoApi.md#VideosLicencesGet) | **Get** /videos/licences | Get list of video licences known by the server
[**VideosOwnershipGet**](VideoApi.md#VideosOwnershipGet) | **Get** /videos/ownership | Get list of video ownership changes requests
[**VideosOwnershipIdAcceptPost**](VideoApi.md#VideosOwnershipIdAcceptPost) | **Post** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
[**VideosOwnershipIdRefusePost**](VideoApi.md#VideosOwnershipIdRefusePost) | **Post** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
[**VideosPrivaciesGet**](VideoApi.md#VideosPrivaciesGet) | **Get** /videos/privacies | Get list of privacy policies supported by the server
[**VideosUploadPost**](VideoApi.md#VideosUploadPost) | **Post** /videos/upload | Upload a video file with its metadata



## AccountsNameVideosGet

> VideoListResponse AccountsNameVideosGet(ctx, name)

Get videos for an account, provided the name of that account

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**name** | **string**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideoChannelsChannelHandleVideosGet

> VideoListResponse VideoChannelsChannelHandleVideosGet(ctx, channelHandle)

Get videos of a video channel by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelHandle** | **string**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosCategoriesGet

> []string VideosCategoriesGet(ctx, )

Get list of video categories known by the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosGet

> VideoListResponse VideosGet(ctx, optional)

Get list of videos

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***VideosGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**optional.Interface of OneOfnumberarray**](.md)| category id of the video | 
 **tagsOneOf** | [**optional.Interface of OneOfstringarray**](.md)| tag(s) of the video | 
 **tagsAllOf** | [**optional.Interface of OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | 
 **licenceOneOf** | [**optional.Interface of OneOfnumberarray**](.md)| licence id of the video | 
 **languageOneOf** | [**optional.Interface of OneOfstringarray**](.md)| language id of the video | 
 **nsfw** | **optional.String**| whether to include nsfw videos, if any | 
 **filter** | **optional.String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | 
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort videos by criteria | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdDelete

> VideosIdDelete(ctx, id)

Delete a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdDescriptionGet

> string VideosIdDescriptionGet(ctx, id)

Get a video description by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdGet

> VideoDetails VideosIdGet(ctx, id)

Get a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdGiveOwnershipPost

> VideosIdGiveOwnershipPost(ctx, id, username)

Request change of ownership for a video you own, by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
**username** | **string**|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdPut

> VideosIdPut(ctx, id, optional)

Update metadata for a video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
 **optional** | ***VideosIdPutOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosIdPutOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **thumbnailfile** | **optional.Interface of *os.File****optional.*os.File**| Video thumbnail file | 
 **previewfile** | **optional.Interface of *os.File****optional.*os.File**| Video preview file | 
 **category** | **optional.String**| Video category | 
 **licence** | **optional.String**| Video licence | 
 **language** | **optional.String**| Video language | 
 **description** | **optional.String**| Video description | 
 **waitTranscoding** | **optional.String**| Whether or not we wait transcoding before publish the video | 
 **support** | **optional.String**| Text describing how to support the video uploader | 
 **nsfw** | **optional.String**| Whether or not this video contains sensitive content | 
 **name** | **optional.String**| Video name | 
 **tags** | [**optional.Interface of []string**](string.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **optional.String**| Enable or disable comments for this video | 
 **originallyPublishedAt** | **optional.Time**| Date when the content was originally published | 
 **scheduleUpdate** | [**optional.Interface of VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdViewsPost

> VideosIdViewsPost(ctx, id)

Add a view to the video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosIdWatchingPut

> VideosIdWatchingPut(ctx, id, userWatchingVideo)

Set watching progress of a video by its id for a user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 
**userWatchingVideo** | [**UserWatchingVideo**](UserWatchingVideo.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosImportsPost

> VideoUploadResponse VideosImportsPost(ctx, channelId, name, optional)

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **float32**| Channel id that will contain this video | 
**name** | **string**| Video name | 
 **optional** | ***VideosImportsPostOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosImportsPostOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **torrentfile** | **optional.Interface of *os.File****optional.*os.File**| Torrent File | 
 **targetUrl** | **optional.String**| HTTP target URL | 
 **magnetUri** | **optional.String**| Magnet URI | 
 **thumbnailfile** | **optional.Interface of *os.File****optional.*os.File**| Video thumbnail file | 
 **previewfile** | **optional.Interface of *os.File****optional.*os.File**| Video preview file | 
 **privacy** | [**optional.Interface of VideoPrivacySet**](VideoPrivacySet.md)|  | 
 **category** | **optional.String**| Video category | 
 **licence** | **optional.String**| Video licence | 
 **language** | **optional.String**| Video language | 
 **description** | **optional.String**| Video description | 
 **waitTranscoding** | **optional.String**| Whether or not we wait transcoding before publish the video | 
 **support** | **optional.String**| Text describing how to support the video uploader | 
 **nsfw** | **optional.String**| Whether or not this video contains sensitive content | 
 **tags** | [**optional.Interface of []string**](string.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **optional.String**| Enable or disable comments for this video | 
 **scheduleUpdate** | [**optional.Interface of VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosLanguagesGet

> []string VideosLanguagesGet(ctx, )

Get list of languages known by the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosLicencesGet

> []string VideosLicencesGet(ctx, )

Get list of video licences known by the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosOwnershipGet

> VideosOwnershipGet(ctx, )

Get list of video ownership changes requests

### Required Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosOwnershipIdAcceptPost

> VideosOwnershipIdAcceptPost(ctx, id)

Refuse ownership change request for video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosOwnershipIdRefusePost

> VideosOwnershipIdRefusePost(ctx, id)

Accept ownership change request for video by its id

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string**| The video id or uuid | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosPrivaciesGet

> []string VideosPrivaciesGet(ctx, )

Get list of privacy policies supported by the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## VideosUploadPost

> VideoUploadResponse VideosUploadPost(ctx, videofile, channelId, name, optional)

Upload a video file with its metadata

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**videofile** | ***os.File*****os.File**| Video file | 
**channelId** | **float32**| Channel id that will contain this video | 
**name** | **string**| Video name | 
 **optional** | ***VideosUploadPostOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a VideosUploadPostOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **thumbnailfile** | **optional.Interface of *os.File****optional.*os.File**| Video thumbnail file | 
 **previewfile** | **optional.Interface of *os.File****optional.*os.File**| Video preview file | 
 **privacy** | [**optional.Interface of VideoPrivacySet**](VideoPrivacySet.md)|  | 
 **category** | **optional.String**| Video category | 
 **licence** | **optional.String**| Video licence | 
 **language** | **optional.String**| Video language | 
 **description** | **optional.String**| Video description | 
 **waitTranscoding** | **optional.String**| Whether or not we wait transcoding before publish the video | 
 **support** | **optional.String**| Text describing how to support the video uploader | 
 **nsfw** | **optional.String**| Whether or not this video contains sensitive content | 
 **tags** | [**optional.Interface of []string**](string.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | 
 **commentsEnabled** | **optional.String**| Enable or disable comments for this video | 
 **originallyPublishedAt** | **optional.Time**| Date when the content was originally published | 
 **scheduleUpdate** | [**optional.Interface of VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

