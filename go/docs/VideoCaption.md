# VideoCaption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**CaptionPath** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


