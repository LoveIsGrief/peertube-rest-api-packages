# VideoDetailsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DescriptionPath** | **string** |  | [optional] 
**Support** | **string** |  | [optional] 
**Channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**Account** | [**Account**](Account.md) |  | [optional] 
**Tags** | **[]string** |  | [optional] 
**Files** | [**[]VideoFile**](VideoFile.md) |  | [optional] 
**CommentsEnabled** | **bool** |  | [optional] 
**DownloadEnabled** | **bool** |  | [optional] 
**TrackerUrls** | **[]string** |  | [optional] 
**StreamingPlaylists** | [**[]VideoStreamingPlaylists**](VideoStreamingPlaylists.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


