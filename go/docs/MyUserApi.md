# \MyUserApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UsersMeAvatarPickPost**](MyUserApi.md#UsersMeAvatarPickPost) | **Post** /users/me/avatar/pick | Update current user avatar
[**UsersMeGet**](MyUserApi.md#UsersMeGet) | **Get** /users/me | Get current user information
[**UsersMePut**](MyUserApi.md#UsersMePut) | **Put** /users/me | Update current user information
[**UsersMeSubscriptionsExistGet**](MyUserApi.md#UsersMeSubscriptionsExistGet) | **Get** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
[**UsersMeSubscriptionsGet**](MyUserApi.md#UsersMeSubscriptionsGet) | **Get** /users/me/subscriptions | Get subscriptions of the current user
[**UsersMeSubscriptionsPost**](MyUserApi.md#UsersMeSubscriptionsPost) | **Post** /users/me/subscriptions | Add subscription to the current user
[**UsersMeSubscriptionsSubscriptionHandleDelete**](MyUserApi.md#UsersMeSubscriptionsSubscriptionHandleDelete) | **Delete** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
[**UsersMeSubscriptionsSubscriptionHandleGet**](MyUserApi.md#UsersMeSubscriptionsSubscriptionHandleGet) | **Get** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
[**UsersMeSubscriptionsVideosGet**](MyUserApi.md#UsersMeSubscriptionsVideosGet) | **Get** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
[**UsersMeVideoQuotaUsedGet**](MyUserApi.md#UsersMeVideoQuotaUsedGet) | **Get** /users/me/video-quota-used | Get current user used quota
[**UsersMeVideosGet**](MyUserApi.md#UsersMeVideosGet) | **Get** /users/me/videos | Get videos of the current user
[**UsersMeVideosImportsGet**](MyUserApi.md#UsersMeVideosImportsGet) | **Get** /users/me/videos/imports | Get video imports of current user
[**UsersMeVideosVideoIdRatingGet**](MyUserApi.md#UsersMeVideosVideoIdRatingGet) | **Get** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user



## UsersMeAvatarPickPost

> Avatar UsersMeAvatarPickPost(ctx, optional)

Update current user avatar

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UsersMeAvatarPickPostOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UsersMeAvatarPickPostOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | **optional.Interface of *os.File****optional.*os.File**| The file to upload. | 

### Return type

[**Avatar**](Avatar.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeGet

> []User UsersMeGet(ctx, )

Get current user information

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**[]User**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMePut

> UsersMePut(ctx, updateMe)

Update current user information

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**updateMe** | [**UpdateMe**](UpdateMe.md)|  | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsExistGet

> map[string]interface{} UsersMeSubscriptionsExistGet(ctx, uris)

Get if subscriptions exist for the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**uris** | [**[]string**](string.md)| list of uris to check if each is part of the user subscriptions | 

### Return type

[**map[string]interface{}**](map[string]interface{}.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsGet

> UsersMeSubscriptionsGet(ctx, optional)

Get subscriptions of the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UsersMeSubscriptionsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UsersMeSubscriptionsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsPost

> UsersMeSubscriptionsPost(ctx, )

Add subscription to the current user

### Required Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsSubscriptionHandleDelete

> UsersMeSubscriptionsSubscriptionHandleDelete(ctx, subscriptionHandle)

Delete subscription of the current user for a given uri

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionHandle** | **string**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsSubscriptionHandleGet

> VideoChannel UsersMeSubscriptionsSubscriptionHandleGet(ctx, subscriptionHandle)

Get subscription of the current user for a given uri

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionHandle** | **string**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeSubscriptionsVideosGet

> VideoListResponse UsersMeSubscriptionsVideosGet(ctx, optional)

Get videos of subscriptions of the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UsersMeSubscriptionsVideosGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UsersMeSubscriptionsVideosGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideoQuotaUsedGet

> float32 UsersMeVideoQuotaUsedGet(ctx, )

Get current user used quota

### Required Parameters

This endpoint does not need any parameter.

### Return type

**float32**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosGet

> VideoListResponse UsersMeVideosGet(ctx, optional)

Get videos of the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UsersMeVideosGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UsersMeVideosGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosImportsGet

> VideoImport UsersMeVideosImportsGet(ctx, optional)

Get video imports of current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UsersMeVideosImportsGetOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UsersMeVideosImportsGetOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **optional.Float32**| Offset | 
 **count** | **optional.Float32**| Number of items | 
 **sort** | **optional.String**| Sort column (-createdAt for example) | 

### Return type

[**VideoImport**](VideoImport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UsersMeVideosVideoIdRatingGet

> GetMeVideoRating UsersMeVideosVideoIdRatingGet(ctx, videoId)

Get rating of video by its id, among those of the current user

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**videoId** | **string**| The video id  | 

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

