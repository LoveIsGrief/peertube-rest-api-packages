# VideoDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Uuid** | **string** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**PublishedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**OriginallyPublishedAt** | **string** |  | [optional] 
**Category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**Licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**Language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**Privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  | [optional] 
**Description** | **string** |  | [optional] 
**Duration** | **float32** |  | [optional] 
**IsLocal** | **bool** |  | [optional] 
**Name** | **string** |  | [optional] 
**ThumbnailPath** | **string** |  | [optional] 
**PreviewPath** | **string** |  | [optional] 
**EmbedPath** | **string** |  | [optional] 
**Views** | **float32** |  | [optional] 
**Likes** | **float32** |  | [optional] 
**Dislikes** | **float32** |  | [optional] 
**Nsfw** | **bool** |  | [optional] 
**WaitTranscoding** | Pointer to **bool** |  | [optional] 
**State** | [**VideoStateConstant**](VideoStateConstant.md) |  | [optional] 
**ScheduledUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 
**Blacklisted** | Pointer to **bool** |  | [optional] 
**BlacklistedReason** | Pointer to **string** |  | [optional] 
**Account** | [**Account**](Account.md) |  | [optional] 
**Channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**UserHistory** | Pointer to [**VideoUserHistory**](Video_userHistory.md) |  | [optional] 
**DescriptionPath** | **string** |  | [optional] 
**Support** | **string** |  | [optional] 
**Tags** | **[]string** |  | [optional] 
**Files** | [**[]VideoFile**](VideoFile.md) |  | [optional] 
**CommentsEnabled** | **bool** |  | [optional] 
**DownloadEnabled** | **bool** |  | [optional] 
**TrackerUrls** | **[]string** |  | [optional] 
**StreamingPlaylists** | [**[]VideoStreamingPlaylists**](VideoStreamingPlaylists.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


