# VideoChannelUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Support** | **string** |  | [optional] 
**BulkVideosSupportUpdate** | **bool** | Update all videos support field of this channel | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


