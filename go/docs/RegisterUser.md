# RegisterUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | The username of the user  | 
**Password** | **string** | The password of the user  | 
**Email** | **string** | The email of the user  | 
**DisplayName** | **string** | The user display name | [optional] 
**Channel** | [**RegisterUserChannel**](RegisterUser_channel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


