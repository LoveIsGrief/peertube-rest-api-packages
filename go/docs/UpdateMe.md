# UpdateMe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Password** | **string** | Your new password  | 
**Email** | **string** | Your new email  | 
**DisplayNSFW** | **string** | Your new displayNSFW  | 
**AutoPlayVideo** | **string** | Your new autoPlayVideo  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


