# VideoBlacklist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**VideoId** | **float32** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Uuid** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Duration** | **float32** |  | [optional] 
**Views** | **float32** |  | [optional] 
**Likes** | **float32** |  | [optional] 
**Dislikes** | **float32** |  | [optional] 
**Nsfw** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


