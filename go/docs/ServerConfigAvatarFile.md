# ServerConfigAvatarFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Size** | [**ServerConfigAvatarFileSize**](ServerConfig_avatar_file_size.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


