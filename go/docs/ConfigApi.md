# \ConfigApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ConfigAboutGet**](ConfigApi.md#ConfigAboutGet) | **Get** /config/about | Get the instance about page content
[**ConfigCustomDelete**](ConfigApi.md#ConfigCustomDelete) | **Delete** /config/custom | Delete the runtime configuration of the server
[**ConfigCustomGet**](ConfigApi.md#ConfigCustomGet) | **Get** /config/custom | Get the runtime configuration of the server
[**ConfigCustomPut**](ConfigApi.md#ConfigCustomPut) | **Put** /config/custom | Set the runtime configuration of the server
[**ConfigGet**](ConfigApi.md#ConfigGet) | **Get** /config | Get the public configuration of the server



## ConfigAboutGet

> ServerConfigAbout ConfigAboutGet(ctx, )

Get the instance about page content

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfigAbout**](ServerConfigAbout.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConfigCustomDelete

> ConfigCustomDelete(ctx, )

Delete the runtime configuration of the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConfigCustomGet

> ServerConfigCustom ConfigCustomGet(ctx, )

Get the runtime configuration of the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfigCustom**](ServerConfigCustom.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConfigCustomPut

> ConfigCustomPut(ctx, )

Set the runtime configuration of the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConfigGet

> ServerConfig ConfigGet(ctx, )

Get the public configuration of the server

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfig**](ServerConfig.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

