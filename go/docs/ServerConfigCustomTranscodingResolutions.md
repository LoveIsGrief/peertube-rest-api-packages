# ServerConfigCustomTranscodingResolutions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Var240p** | **bool** |  | [optional] 
**Var360p** | **bool** |  | [optional] 
**Var480p** | **bool** |  | [optional] 
**Var720p** | **bool** |  | [optional] 
**Var1080p** | **bool** |  | [optional] 
**Var2160p** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


