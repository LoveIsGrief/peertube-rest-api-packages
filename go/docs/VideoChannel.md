# VideoChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**IsLocal** | **bool** |  | [optional] 
**OwnerAccount** | [**VideoChannelOwnerAccount**](VideoChannel_ownerAccount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


