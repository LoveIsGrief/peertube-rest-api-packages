# ServerConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Instance** | [**ServerConfigInstance**](ServerConfig_instance.md) |  | [optional] 
**Plugin** | [**ServerConfigPlugin**](ServerConfig_plugin.md) |  | [optional] 
**Theme** | [**ServerConfigPlugin**](ServerConfig_plugin.md) |  | [optional] 
**Email** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 
**ContactForm** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 
**ServerVersion** | **string** |  | [optional] 
**ServerCommit** | **string** |  | [optional] 
**Signup** | [**ServerConfigSignup**](ServerConfig_signup.md) |  | [optional] 
**Transcoding** | [**ServerConfigTranscoding**](ServerConfig_transcoding.md) |  | [optional] 
**Import** | [**ServerConfigImport**](ServerConfig_import.md) |  | [optional] 
**AutoBlacklist** | [**ServerConfigAutoBlacklist**](ServerConfig_autoBlacklist.md) |  | [optional] 
**Avatar** | [**ServerConfigAvatar**](ServerConfig_avatar.md) |  | [optional] 
**Video** | [**ServerConfigVideo**](ServerConfig_video.md) |  | [optional] 
**VideoCaption** | [**ServerConfigVideoCaption**](ServerConfig_videoCaption.md) |  | [optional] 
**User** | [**ServerConfigUser**](ServerConfig_user.md) |  | [optional] 
**Trending** | [**ServerConfigTrending**](ServerConfig_trending.md) |  | [optional] 
**Tracker** | [**ServerConfigEmail**](ServerConfig_email.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


