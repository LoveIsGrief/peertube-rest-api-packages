# ServerConfigCustomCache

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Previews** | [**ServerConfigCustomCachePreviews**](ServerConfigCustom_cache_previews.md) |  | [optional] 
**Captions** | [**ServerConfigCustomCachePreviews**](ServerConfigCustom_cache_previews.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


