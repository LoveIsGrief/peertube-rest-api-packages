# VideoComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **float32** |  | [optional] 
**Url** | **string** |  | [optional] 
**Text** | **string** |  | [optional] 
**ThreadId** | **float32** |  | [optional] 
**InReplyToCommentId** | **float32** |  | [optional] 
**VideoId** | **float32** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**TotalRepliesFromVideoAuthor** | **float32** |  | [optional] 
**TotalReplies** | **float32** |  | [optional] 
**Account** | [**Account**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


