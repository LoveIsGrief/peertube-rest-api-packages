# ServerConfigTrending

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Videos** | [**ServerConfigTrendingVideos**](ServerConfig_trending_videos.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


