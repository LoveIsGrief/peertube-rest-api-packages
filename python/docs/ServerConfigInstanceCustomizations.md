# ServerConfigInstanceCustomizations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**javascript** | **str** |  | [optional] 
**css** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


