# peertube.MyUserApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_avatar_pick_post**](MyUserApi.md#users_me_avatar_pick_post) | **POST** /users/me/avatar/pick | Update current user avatar
[**users_me_get**](MyUserApi.md#users_me_get) | **GET** /users/me | Get current user information
[**users_me_put**](MyUserApi.md#users_me_put) | **PUT** /users/me | Update current user information
[**users_me_subscriptions_exist_get**](MyUserApi.md#users_me_subscriptions_exist_get) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
[**users_me_subscriptions_get**](MyUserApi.md#users_me_subscriptions_get) | **GET** /users/me/subscriptions | Get subscriptions of the current user
[**users_me_subscriptions_post**](MyUserApi.md#users_me_subscriptions_post) | **POST** /users/me/subscriptions | Add subscription to the current user
[**users_me_subscriptions_subscription_handle_delete**](MyUserApi.md#users_me_subscriptions_subscription_handle_delete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
[**users_me_subscriptions_subscription_handle_get**](MyUserApi.md#users_me_subscriptions_subscription_handle_get) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
[**users_me_subscriptions_videos_get**](MyUserApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
[**users_me_video_quota_used_get**](MyUserApi.md#users_me_video_quota_used_get) | **GET** /users/me/video-quota-used | Get current user used quota
[**users_me_videos_get**](MyUserApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of the current user
[**users_me_videos_imports_get**](MyUserApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of current user
[**users_me_videos_video_id_rating_get**](MyUserApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user


# **users_me_avatar_pick_post**
> Avatar users_me_avatar_pick_post(avatarfile=avatarfile)

Update current user avatar

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    avatarfile = '/path/to/file' # file | The file to upload. (optional)

    try:
        # Update current user avatar
        api_response = api_instance.users_me_avatar_pick_post(avatarfile=avatarfile)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_avatar_pick_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | **file**| The file to upload. | [optional] 

### Return type

[**Avatar**](Avatar.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_get**
> list[User] users_me_get()

Get current user information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    
    try:
        # Get current user information
        api_response = api_instance.users_me_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[User]**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_put**
> users_me_put(update_me)

Update current user information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    update_me = peertube.UpdateMe() # UpdateMe | 

    try:
        # Update current user information
        api_instance.users_me_put(update_me)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_me** | [**UpdateMe**](UpdateMe.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_exist_get**
> object users_me_subscriptions_exist_get(uris)

Get if subscriptions exist for the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    uris = ['uris_example'] # list[str] | list of uris to check if each is part of the user subscriptions

    try:
        # Get if subscriptions exist for the current user
        api_response = api_instance.users_me_subscriptions_exist_get(uris)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_exist_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uris** | [**list[str]**](str.md)| list of uris to check if each is part of the user subscriptions | 

### Return type

**object**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_get**
> users_me_subscriptions_get(start=start, count=count, sort=sort)

Get subscriptions of the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get subscriptions of the current user
        api_instance.users_me_subscriptions_get(start=start, count=count, sort=sort)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_post**
> users_me_subscriptions_post()

Add subscription to the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    
    try:
        # Add subscription to the current user
        api_instance.users_me_subscriptions_post()
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_post: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_subscription_handle_delete**
> users_me_subscriptions_subscription_handle_delete(subscription_handle)

Delete subscription of the current user for a given uri

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    subscription_handle = 'subscription_handle_example' # str | The subscription handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Delete subscription of the current user for a given uri
        api_instance.users_me_subscriptions_subscription_handle_delete(subscription_handle)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_subscription_handle_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **str**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_subscription_handle_get**
> VideoChannel users_me_subscriptions_subscription_handle_get(subscription_handle)

Get subscription of the current user for a given uri

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    subscription_handle = 'subscription_handle_example' # str | The subscription handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Get subscription of the current user for a given uri
        api_response = api_instance.users_me_subscriptions_subscription_handle_get(subscription_handle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_subscription_handle_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **str**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_videos_get**
> VideoListResponse users_me_subscriptions_videos_get(start=start, count=count, sort=sort)

Get videos of subscriptions of the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get videos of subscriptions of the current user
        api_response = api_instance.users_me_subscriptions_videos_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_subscriptions_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_video_quota_used_get**
> float users_me_video_quota_used_get()

Get current user used quota

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    
    try:
        # Get current user used quota
        api_response = api_instance.users_me_video_quota_used_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_video_quota_used_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**float**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_videos_get**
> VideoListResponse users_me_videos_get(start=start, count=count, sort=sort)

Get videos of the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get videos of the current user
        api_response = api_instance.users_me_videos_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_videos_imports_get**
> VideoImport users_me_videos_imports_get(start=start, count=count, sort=sort)

Get video imports of current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get video imports of current user
        api_response = api_instance.users_me_videos_imports_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_videos_imports_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

[**VideoImport**](VideoImport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_videos_video_id_rating_get**
> GetMeVideoRating users_me_videos_video_id_rating_get(video_id)

Get rating of video by its id, among those of the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyUserApi(api_client)
    video_id = 'video_id_example' # str | The video id 

    try:
        # Get rating of video by its id, among those of the current user
        api_response = api_instance.users_me_videos_video_id_rating_get(video_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyUserApi->users_me_videos_video_id_rating_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_id** | **str**| The video id  | 

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

