# PlaylistElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **float** |  | [optional] 
**start_timestamp** | **float** |  | [optional] 
**stop_timestamp** | **float** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


