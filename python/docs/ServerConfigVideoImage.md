# ServerConfigVideoImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extensions** | **list[str]** |  | [optional] 
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


