# peertube.AccountsApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_get**](AccountsApi.md#accounts_get) | **GET** /accounts | Get all accounts
[**accounts_name_get**](AccountsApi.md#accounts_name_get) | **GET** /accounts/{name} | Get the account by name
[**accounts_name_videos_get**](AccountsApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account


# **accounts_get**
> list[Account] accounts_get(start=start, count=count, sort=sort)

Get all accounts

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get all accounts
        api_response = api_instance.accounts_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

[**list[Account]**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_get**
> Account accounts_name_get(name)

Get the account by name

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'name_example' # str | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)

    try:
        # Get the account by name
        api_response = api_instance.accounts_name_get(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_videos_get**
> VideoListResponse accounts_name_videos_get(name)

Get videos for an account, provided the name of that account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'name_example' # str | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)

    try:
        # Get videos for an account, provided the name of that account
        api_response = api_instance.accounts_name_videos_get(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

