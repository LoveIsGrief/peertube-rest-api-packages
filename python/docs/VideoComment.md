# VideoComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**url** | **str** |  | [optional] 
**text** | **str** |  | [optional] 
**thread_id** | **float** |  | [optional] 
**in_reply_to_comment_id** | **float** |  | [optional] 
**video_id** | **float** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**total_replies_from_video_author** | **float** |  | [optional] 
**total_replies** | **float** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


