# VideoFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**magnet_uri** | **str** |  | [optional] 
**resolution** | [**VideoResolutionConstant**](VideoResolutionConstant.md) |  | [optional] 
**size** | **float** | Video file size in bytes | [optional] 
**torrent_url** | **str** |  | [optional] 
**torrent_download_url** | **str** |  | [optional] 
**file_url** | **str** |  | [optional] 
**file_download_url** | **str** |  | [optional] 
**fps** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


