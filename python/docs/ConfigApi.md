# peertube.ConfigApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**config_about_get**](ConfigApi.md#config_about_get) | **GET** /config/about | Get the instance about page content
[**config_custom_delete**](ConfigApi.md#config_custom_delete) | **DELETE** /config/custom | Delete the runtime configuration of the server
[**config_custom_get**](ConfigApi.md#config_custom_get) | **GET** /config/custom | Get the runtime configuration of the server
[**config_custom_put**](ConfigApi.md#config_custom_put) | **PUT** /config/custom | Set the runtime configuration of the server
[**config_get**](ConfigApi.md#config_get) | **GET** /config | Get the public configuration of the server


# **config_about_get**
> ServerConfigAbout config_about_get()

Get the instance about page content

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.ConfigApi(api_client)
    
    try:
        # Get the instance about page content
        api_response = api_instance.config_about_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigApi->config_about_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigAbout**](ServerConfigAbout.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_custom_delete**
> config_custom_delete()

Delete the runtime configuration of the server

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ConfigApi(api_client)
    
    try:
        # Delete the runtime configuration of the server
        api_instance.config_custom_delete()
    except ApiException as e:
        print("Exception when calling ConfigApi->config_custom_delete: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_custom_get**
> ServerConfigCustom config_custom_get()

Get the runtime configuration of the server

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ConfigApi(api_client)
    
    try:
        # Get the runtime configuration of the server
        api_response = api_instance.config_custom_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigApi->config_custom_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigCustom**](ServerConfigCustom.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_custom_put**
> config_custom_put()

Set the runtime configuration of the server

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ConfigApi(api_client)
    
    try:
        # Set the runtime configuration of the server
        api_instance.config_custom_put()
    except ApiException as e:
        print("Exception when calling ConfigApi->config_custom_put: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_get**
> ServerConfig config_get()

Get the public configuration of the server

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.ConfigApi(api_client)
    
    try:
        # Get the public configuration of the server
        api_response = api_instance.config_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ConfigApi->config_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfig**](ServerConfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

