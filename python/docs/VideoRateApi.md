# peertube.VideoRateApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_rate_put**](VideoRateApi.md#videos_id_rate_put) | **PUT** /videos/{id}/rate | Vote for a video by its id


# **videos_id_rate_put**
> videos_id_rate_put(id)

Vote for a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoRateApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Vote for a video by its id
        api_instance.videos_id_rate_put(id)
    except ApiException as e:
        print("Exception when calling VideoRateApi->videos_id_rate_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

