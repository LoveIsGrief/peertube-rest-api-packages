# ServerConfigCustomInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**short_description** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**terms** | **str** |  | [optional] 
**default_client_route** | **str** |  | [optional] 
**is_nsfw** | **bool** |  | [optional] 
**default_nsfw_policy** | **str** |  | [optional] 
**customizations** | [**ServerConfigInstanceCustomizations**](ServerConfigInstanceCustomizations.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


