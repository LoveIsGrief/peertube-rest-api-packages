# peertube.VideoChannelApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_name_video_channels_get**](VideoChannelApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | Get video channels of an account by its name
[**video_channels_channel_handle_delete**](VideoChannelApi.md#video_channels_channel_handle_delete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel by its id
[**video_channels_channel_handle_get**](VideoChannelApi.md#video_channels_channel_handle_get) | **GET** /video-channels/{channelHandle} | Get a video channel by its id
[**video_channels_channel_handle_put**](VideoChannelApi.md#video_channels_channel_handle_put) | **PUT** /video-channels/{channelHandle} | Update a video channel by its id
[**video_channels_channel_handle_videos_get**](VideoChannelApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**video_channels_get**](VideoChannelApi.md#video_channels_get) | **GET** /video-channels | Get list of video channels
[**video_channels_post**](VideoChannelApi.md#video_channels_post) | **POST** /video-channels | Creates a video channel for the current user


# **accounts_name_video_channels_get**
> list[VideoChannel] accounts_name_video_channels_get(name)

Get video channels of an account by its name

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    name = 'name_example' # str | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)

    try:
        # Get video channels of an account by its name
        api_response = api_instance.accounts_name_video_channels_get(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->accounts_name_video_channels_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**list[VideoChannel]**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_delete**
> video_channels_channel_handle_delete(channel_handle)

Delete a video channel by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    channel_handle = 'channel_handle_example' # str | The video channel handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Delete a video channel by its id
        api_instance.video_channels_channel_handle_delete(channel_handle)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_channel_handle_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_get**
> VideoChannel video_channels_channel_handle_get(channel_handle)

Get a video channel by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    channel_handle = 'channel_handle_example' # str | The video channel handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Get a video channel by its id
        api_response = api_instance.video_channels_channel_handle_get(channel_handle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_channel_handle_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_put**
> video_channels_channel_handle_put(channel_handle, video_channel_update=video_channel_update)

Update a video channel by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    channel_handle = 'channel_handle_example' # str | The video channel handle (example: 'my_username@example.com' or 'my_username')
video_channel_update = peertube.VideoChannelUpdate() # VideoChannelUpdate |  (optional)

    try:
        # Update a video channel by its id
        api_instance.video_channels_channel_handle_put(channel_handle, video_channel_update=video_channel_update)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_channel_handle_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 
 **video_channel_update** | [**VideoChannelUpdate**](VideoChannelUpdate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_videos_get**
> VideoListResponse video_channels_channel_handle_videos_get(channel_handle)

Get videos of a video channel by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    channel_handle = 'channel_handle_example' # str | The video channel handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Get videos of a video channel by its id
        api_response = api_instance.video_channels_channel_handle_videos_get(channel_handle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_channel_handle_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_get**
> list[VideoChannel] video_channels_get(start=start, count=count, sort=sort)

Get list of video channels

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get list of video channels
        api_response = api_instance.video_channels_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort column (-createdAt for example) | [optional] 

### Return type

[**list[VideoChannel]**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_post**
> video_channels_post(video_channel_create=video_channel_create)

Creates a video channel for the current user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoChannelApi(api_client)
    video_channel_create = peertube.VideoChannelCreate() # VideoChannelCreate |  (optional)

    try:
        # Creates a video channel for the current user
        api_instance.video_channels_post(video_channel_create=video_channel_create)
    except ApiException as e:
        print("Exception when calling VideoChannelApi->video_channels_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_channel_create** | [**VideoChannelCreate**](VideoChannelCreate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

