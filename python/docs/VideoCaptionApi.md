# peertube.VideoCaptionApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_captions_caption_language_delete**](VideoCaptionApi.md#videos_id_captions_caption_language_delete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**videos_id_captions_caption_language_put**](VideoCaptionApi.md#videos_id_captions_caption_language_put) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**videos_id_captions_get**](VideoCaptionApi.md#videos_id_captions_get) | **GET** /videos/{id}/captions | Get list of video&#39;s captions


# **videos_id_captions_caption_language_delete**
> videos_id_captions_caption_language_delete(id, caption_language)

Delete a video caption

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionApi(api_client)
    id = 'id_example' # str | The video id or uuid
caption_language = 'caption_language_example' # str | The caption language

    try:
        # Delete a video caption
        api_instance.videos_id_captions_caption_language_delete(id, caption_language)
    except ApiException as e:
        print("Exception when calling VideoCaptionApi->videos_id_captions_caption_language_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **caption_language** | **str**| The caption language | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_captions_caption_language_put**
> videos_id_captions_caption_language_put(id, caption_language, captionfile=captionfile)

Add or replace a video caption

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionApi(api_client)
    id = 'id_example' # str | The video id or uuid
caption_language = 'caption_language_example' # str | The caption language
captionfile = '/path/to/file' # file | The file to upload. (optional)

    try:
        # Add or replace a video caption
        api_instance.videos_id_captions_caption_language_put(id, caption_language, captionfile=captionfile)
    except ApiException as e:
        print("Exception when calling VideoCaptionApi->videos_id_captions_caption_language_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **caption_language** | **str**| The caption language | 
 **captionfile** | **file**| The file to upload. | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_captions_get**
> InlineResponse200 videos_id_captions_get(id)

Get list of video's captions

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Get list of video's captions
        api_response = api_instance.videos_id_captions_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCaptionApi->videos_id_captions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

