# ServerConfigUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_quota** | **float** |  | [optional] 
**video_quota_daily** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


