# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**username** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**display_nsfw** | **bool** |  | [optional] 
**auto_play_video** | **bool** |  | [optional] 
**role** | **int** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | [optional] 
**role_label** | **str** |  | [optional] 
**video_quota** | **float** |  | [optional] 
**video_quota_daily** | **float** |  | [optional] 
**created_at** | **str** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**video_channels** | [**list[VideoChannel]**](VideoChannel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


