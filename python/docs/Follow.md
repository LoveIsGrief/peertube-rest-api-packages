# Follow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**follower** | [**Actor**](Actor.md) |  | [optional] 
**following** | [**Actor**](Actor.md) |  | [optional] 
**score** | **float** |  | [optional] 
**state** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


