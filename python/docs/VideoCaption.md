# VideoCaption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**caption_path** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


