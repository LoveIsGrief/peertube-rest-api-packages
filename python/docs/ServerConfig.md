# ServerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigInstance**](ServerConfigInstance.md) |  | [optional] 
**plugin** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  | [optional] 
**theme** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  | [optional] 
**email** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**contact_form** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**server_version** | **str** |  | [optional] 
**server_commit** | **str** |  | [optional] 
**signup** | [**ServerConfigSignup**](ServerConfigSignup.md) |  | [optional] 
**transcoding** | [**ServerConfigTranscoding**](ServerConfigTranscoding.md) |  | [optional] 
**_import** | [**ServerConfigImport**](ServerConfigImport.md) |  | [optional] 
**auto_blacklist** | [**ServerConfigAutoBlacklist**](ServerConfigAutoBlacklist.md) |  | [optional] 
**avatar** | [**ServerConfigAvatar**](ServerConfigAvatar.md) |  | [optional] 
**video** | [**ServerConfigVideo**](ServerConfigVideo.md) |  | [optional] 
**video_caption** | [**ServerConfigVideoCaption**](ServerConfigVideoCaption.md) |  | [optional] 
**user** | [**ServerConfigUser**](ServerConfigUser.md) |  | [optional] 
**trending** | [**ServerConfigTrending**](ServerConfigTrending.md) |  | [optional] 
**tracker** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


