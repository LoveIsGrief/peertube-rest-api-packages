# AddUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | The user username  | 
**password** | **str** | The user password  | 
**email** | **str** | The user email  | 
**video_quota** | **str** | The user videoQuota  | 
**video_quota_daily** | **str** | The user daily video quota  | 
**role** | **int** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


