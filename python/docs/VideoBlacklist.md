# VideoBlacklist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**video_id** | **float** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**duration** | **float** |  | [optional] 
**views** | **float** |  | [optional] 
**likes** | **float** |  | [optional] 
**dislikes** | **float** |  | [optional] 
**nsfw** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


