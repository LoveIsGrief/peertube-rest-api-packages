# UpdateUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The user id  | 
**email** | **str** | The updated email of the user  | 
**video_quota** | **str** | The updated videoQuota of the user  | 
**video_quota_daily** | **str** | The updated daily video quota of the user  | 
**role** | **int** | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


