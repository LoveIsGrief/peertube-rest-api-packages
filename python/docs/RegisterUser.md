# RegisterUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | The username of the user  | 
**password** | **str** | The password of the user  | 
**email** | **str** | The email of the user  | 
**display_name** | **str** | The user display name | [optional] 
**channel** | [**RegisterUserChannel**](RegisterUserChannel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


