# VideoDetailsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description_path** | **str** |  | [optional] 
**support** | **str** |  | [optional] 
**channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**tags** | **list[str]** |  | [optional] 
**files** | [**list[VideoFile]**](VideoFile.md) |  | [optional] 
**comments_enabled** | **bool** |  | [optional] 
**download_enabled** | **bool** |  | [optional] 
**tracker_urls** | **list[str]** |  | [optional] 
**streaming_playlists** | [**list[VideoStreamingPlaylists]**](VideoStreamingPlaylists.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


