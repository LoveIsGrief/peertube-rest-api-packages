# coding: utf-8

"""
    PeerTube

    # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ```   # noqa: E501

    The version of the OpenAPI document: 2.0.0
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from peertube.configuration import Configuration


class VideoBlacklist(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'float',
        'video_id': 'float',
        'created_at': 'str',
        'updated_at': 'str',
        'name': 'str',
        'uuid': 'str',
        'description': 'str',
        'duration': 'float',
        'views': 'float',
        'likes': 'float',
        'dislikes': 'float',
        'nsfw': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'video_id': 'videoId',
        'created_at': 'createdAt',
        'updated_at': 'updatedAt',
        'name': 'name',
        'uuid': 'uuid',
        'description': 'description',
        'duration': 'duration',
        'views': 'views',
        'likes': 'likes',
        'dislikes': 'dislikes',
        'nsfw': 'nsfw'
    }

    def __init__(self, id=None, video_id=None, created_at=None, updated_at=None, name=None, uuid=None, description=None, duration=None, views=None, likes=None, dislikes=None, nsfw=None, local_vars_configuration=None):  # noqa: E501
        """VideoBlacklist - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._video_id = None
        self._created_at = None
        self._updated_at = None
        self._name = None
        self._uuid = None
        self._description = None
        self._duration = None
        self._views = None
        self._likes = None
        self._dislikes = None
        self._nsfw = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if video_id is not None:
            self.video_id = video_id
        if created_at is not None:
            self.created_at = created_at
        if updated_at is not None:
            self.updated_at = updated_at
        if name is not None:
            self.name = name
        if uuid is not None:
            self.uuid = uuid
        if description is not None:
            self.description = description
        if duration is not None:
            self.duration = duration
        if views is not None:
            self.views = views
        if likes is not None:
            self.likes = likes
        if dislikes is not None:
            self.dislikes = dislikes
        if nsfw is not None:
            self.nsfw = nsfw

    @property
    def id(self):
        """Gets the id of this VideoBlacklist.  # noqa: E501


        :return: The id of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this VideoBlacklist.


        :param id: The id of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._id = id

    @property
    def video_id(self):
        """Gets the video_id of this VideoBlacklist.  # noqa: E501


        :return: The video_id of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._video_id

    @video_id.setter
    def video_id(self, video_id):
        """Sets the video_id of this VideoBlacklist.


        :param video_id: The video_id of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._video_id = video_id

    @property
    def created_at(self):
        """Gets the created_at of this VideoBlacklist.  # noqa: E501


        :return: The created_at of this VideoBlacklist.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this VideoBlacklist.


        :param created_at: The created_at of this VideoBlacklist.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this VideoBlacklist.  # noqa: E501


        :return: The updated_at of this VideoBlacklist.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this VideoBlacklist.


        :param updated_at: The updated_at of this VideoBlacklist.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    @property
    def name(self):
        """Gets the name of this VideoBlacklist.  # noqa: E501


        :return: The name of this VideoBlacklist.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this VideoBlacklist.


        :param name: The name of this VideoBlacklist.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def uuid(self):
        """Gets the uuid of this VideoBlacklist.  # noqa: E501


        :return: The uuid of this VideoBlacklist.  # noqa: E501
        :rtype: str
        """
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        """Sets the uuid of this VideoBlacklist.


        :param uuid: The uuid of this VideoBlacklist.  # noqa: E501
        :type: str
        """

        self._uuid = uuid

    @property
    def description(self):
        """Gets the description of this VideoBlacklist.  # noqa: E501


        :return: The description of this VideoBlacklist.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this VideoBlacklist.


        :param description: The description of this VideoBlacklist.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def duration(self):
        """Gets the duration of this VideoBlacklist.  # noqa: E501


        :return: The duration of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._duration

    @duration.setter
    def duration(self, duration):
        """Sets the duration of this VideoBlacklist.


        :param duration: The duration of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._duration = duration

    @property
    def views(self):
        """Gets the views of this VideoBlacklist.  # noqa: E501


        :return: The views of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._views

    @views.setter
    def views(self, views):
        """Sets the views of this VideoBlacklist.


        :param views: The views of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._views = views

    @property
    def likes(self):
        """Gets the likes of this VideoBlacklist.  # noqa: E501


        :return: The likes of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._likes

    @likes.setter
    def likes(self, likes):
        """Sets the likes of this VideoBlacklist.


        :param likes: The likes of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._likes = likes

    @property
    def dislikes(self):
        """Gets the dislikes of this VideoBlacklist.  # noqa: E501


        :return: The dislikes of this VideoBlacklist.  # noqa: E501
        :rtype: float
        """
        return self._dislikes

    @dislikes.setter
    def dislikes(self, dislikes):
        """Sets the dislikes of this VideoBlacklist.


        :param dislikes: The dislikes of this VideoBlacklist.  # noqa: E501
        :type: float
        """

        self._dislikes = dislikes

    @property
    def nsfw(self):
        """Gets the nsfw of this VideoBlacklist.  # noqa: E501


        :return: The nsfw of this VideoBlacklist.  # noqa: E501
        :rtype: bool
        """
        return self._nsfw

    @nsfw.setter
    def nsfw(self, nsfw):
        """Sets the nsfw of this VideoBlacklist.


        :param nsfw: The nsfw of this VideoBlacklist.  # noqa: E501
        :type: bool
        """

        self._nsfw = nsfw

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, VideoBlacklist):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, VideoBlacklist):
            return True

        return self.to_dict() != other.to_dict()
